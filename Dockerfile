FROM golang

ADD . /go/src/gitlab.com/gpestana/rss_server/
RUN go install gitlab.com/gpestana/rss_server/

ENV PORT 80
ENV OUTPUT_DIR=./src/gitlab.com/gpestana/rss_server/rss

EXPOSE 80
ENTRYPOINT /go/bin/rss_server

