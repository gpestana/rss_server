## RSS server

#### HTTP server for RSS compatible XML files

Rss server exposes static files which are in the ./rss directory. The api
only returns file contents and returns 404 error when the user requests for
a directory

- All the files in ./rss should be XML compatible 
- It is not possible to list the available RSS files in ./rss folder. The user
will be able to access RSS feed only if she knows the file name.

