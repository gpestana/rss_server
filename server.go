// Rss server exposes static files which are in the ./rss directory. The api
// only returns file contents and returns 404 error when the user requests for
// a directory
package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

type justFilesFileSystem struct {
	Fs http.FileSystem
}

func (fs justFilesFileSystem) Open(n string) (http.File, error) {
	f, err := fs.Fs.Open(n)

	if err != nil {
		return nil, err
	}

	stat, err := f.Stat()
	if stat.IsDir() {
		return nil, os.ErrNotExist
	}

	return f, nil
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("Port must be defined (ENV)")
	}

	log.Println("Preparing to listen at port", port)

	d := os.Getenv("OUTPUT_DIR")
	if d == "" {
		log.Println("Serving default folder ('./rss')")
		d = "./rss"
	}

	printServingFiles(d)

	fs := justFilesFileSystem{http.Dir(d)}
	err := http.ListenAndServe(":"+port, http.FileServer(fs))

	if err != nil {
		log.Fatal(err)
	}
}

// printServingFiles reads the contents of the directory the server will expose
// and prints it to the stdout
func printServingFiles(dir string) {
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Serving files:")
	for _, f := range files {
		if strings.HasSuffix(f.Name(), ".xml") {
			log.Println(f.Name())
		}
	}
}
